const fetch = require('node-fetch');
const assert = require('assert')

async function main(city) {
    const api_key = 'b59333039cf6b13cc3ca7ad7a14e8689'//mala practica esto va en .env
    const url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${api_key}`
    try {
        const data = await fetch(url);
        const json = await data.json();
        return {
            ...json,
            'status':data.status
        };
        
    } catch (error) {
        console.log(error);
    }
}
describe('Probar la api Weather',()=>{
    it('Prueba de la API', async ( ) => {
        const data = await main('Rio Grande');
        console.log(data)
        assert.strictEqual(data.status, 200)
    })
    it('Si falla', async () => {
        const data = await main('Wakanda');
        console.log(data)
        assert.strictEqual(data.status, 404)
    })
})